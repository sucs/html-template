# 🚀 SUCS Hello World HTML Template

A simple HTML/CSS biography page using Bootstrap!

## 💻 Setup (for SUCS members!)

After setting up a user account with SUCS simply open a command prompt or terminal and run:
```
ssh USERNAME@sucs.org
```
Where `USERNAME` is your SUCS username. Then clone this repo to somewhere public:
```
git clone https://projects.sucs.org/sucs/html-template public_html/hello
```

Then browse to https://sucs.org/~USERNAME/hello <- replacing `USERNAME` again with your SUCS username.

## ✏️ Contributing

The goal for this template is to be a simple user biography page where you can link to your social media and talk a bit about yourself.

## Live example

You can find the latest live version at https://sucs.org/~kalube/hello !

